package net.igniscraft.bombs;

import net.igniscraft.bombs.bombs.AbstractMineBomb;
import net.igniscraft.bombs.bombs.ActivationType;
import net.igniscraft.bombs.bombs.MineBombHandler;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteract implements Listener {
	
	@EventHandler(ignoreCancelled = false)
	public void onPlayerInteract(PlayerInteractEvent event) {
		ItemStack item = event.getItem();
		if (item == null || item.getType() == Material.AIR) {
			return;
		}
		AbstractMineBomb bomb = MineBombHandler.getBombFromItem(item);
		if (bomb == null) {
			return;
		}
		Action action = event.getAction();
		ActivationType type = bomb.getActivationType();
		if ((action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_AIR) 
				&& type == ActivationType.LEFT_CLICK) {
			bomb.throwBomb(event.getPlayer());
		} else if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK)
				&& type == ActivationType.RIGHT_CLICK) {
			bomb.throwBomb(event.getPlayer());
		} else {
			event.getPlayer().sendMessage(ChatColor.RED + "You aren't using this MineBomb correctly. Please use"
					+ " the lore lines to see how you can activate this type of bomb.");
		}
	}

}
