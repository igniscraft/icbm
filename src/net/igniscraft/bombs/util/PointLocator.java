package net.igniscraft.bombs.util;

import org.bukkit.Location;

import com.sk89q.worldedit.BlockVector;

/**
 * Utility class; used for easy bound checking
 */
public class PointLocator {
	
	private int minX;
	private int maxX;
	private int minY;
	private int maxY;
	private int maxZ;
	private int minZ;
	
	/**
	 * This constructor grabs the x, y, and z values to compare
	 * @param first First BlockVector
	 * @param second Second BlockVector
	 */
	public PointLocator(BlockVector first, BlockVector second) {
		this(first.getBlockX(), second.getBlockX(), first.getBlockY(), second.getBlockY(), first.getBlockZ(), 
				second.getBlockZ());
	}
	
	/**
	 * Construct's objects functionality
	 * @param x X coordinate
	 * @param otherX Another X coordinate
	 * @param y Y coordinate
	 * @param otherY Another Y coordinate
	 * @param z Z coordinate
	 * @param otherZ Another Z coordinate
	 */
	public PointLocator(int x, int otherX, int y, int otherY, int z, int otherZ) {
		if (x >= otherX) {
			maxX = x;
			minX = otherX;
		} else {
			minX = x;
			maxX = otherX;
		}
		if (y >= otherY) {
			maxY = y;
			minY = otherY;
		} else {
			minY = y;
			maxY = otherY;
		}
		if (z >= otherZ) {
			maxZ = z;
			minZ = otherZ;
		} else {
			minZ = z;
			maxZ = otherZ;
		}
	}
	
	/**
	 * Another overload constructor
	 * @param one Location one
	 * @param two Location two
	 */
	public PointLocator(Location one, Location two) {
		this(one.getBlockX(), two.getBlockX(), one.getBlockY(), two.getBlockY(), 
				one.getBlockZ(), two.getBlockZ());
	}
	
	/**
	 * Method checks bounds to see if the location is within said bounds.
	 * @param loc The location to check if it's within the bounds.
	 * @return Whether or not the location is within the bounds
	 */
	public boolean isWithinBounds(Location loc) {
		return (loc.getBlockX() >= minX && loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY && loc.getBlockY() 
				<= maxY) && (loc.getBlockZ() >= minZ && loc.getBlockZ() <= maxZ);
	}
	
	/**
	 * Gets the minimum Z value in the bounds
	 * @return Minimum Z value
	 */
	public int getMinZ() {
		return minZ;
	}
	
	/**
	 * Gets the maximum Z value in the bounds
	 * @return Maximum Z value
	 */
	public int getMaxZ() {
		return maxZ;
	}
	
	/**
	 * Gets the minimum X value in the bounds
	 * @return Minimum X value
	 */
	public int getMinX() {
		return minX;
	}
	
	/**
	 * Gets the maximum X value in the bounds
	 * @return Maximum X value
	 */
	public int getMaxX() {
		return maxX;
	}
	
	/**
	 * Gets the minimum Y value in the bounds
	 * @return Minimum Y value
	 */
	public int getMinY() {
		return minY;
	}
	
	/**
	 * Gets the maximum Y value in the bounds
	 * @return Maximum Y value
	 */
	public int getMaxY() {
		return maxY;
	}

}
