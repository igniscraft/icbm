package net.igniscraft.bombs.util;

import net.igniscraft.bombs.Core;
import net.igniscraft.bombs.bombs.AbstractMineBomb;
import net.igniscraft.bombs.bombs.MineBombHandler;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class MineBombRunnable extends BukkitRunnable {

	private Player player;
	private Item item;
	private AbstractMineBomb mineBomb;
	
	public MineBombRunnable(Player player, Item item, AbstractMineBomb mineBomb) {
		super();
		this.player = player;
		this.item = item;
		this.mineBomb = mineBomb;
		this.runTaskLater(Core.getInstance(), 40L);
	}

	@Override
	public void run() {
		Location location = item.getLocation();
		MineBombHandler.removeProtectedItem(item);
		if (RegionHelper.getRegionAtLocation(location) == null 
				&& RegionHelper.getRegionAtLocation(location.subtract(0, 1, 0)) == null) {
			player.sendMessage(ChatColor.RED + "The MineBomb wasn't thrown in a mine!");
			player.getInventory().addItem(item.getItemStack().clone());
			item.remove();
		} else {
			location.getWorld().createExplosion(location, 0F);
			mineBomb.detonate(player, item.getLocation());
			item.remove();
		}
	}

}
