package net.igniscraft.bombs.util;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BlockDrops {
	
	private BlockDrops() {}
	
	private static final Map<ItemStack, ItemStack> BLOCK_DROPS = new HashMap<ItemStack, ItemStack>() {
		
		private static final long serialVersionUID = 1L;

		{
			put(new ItemStack(Material.STONE, 1), new ItemStack(Material.COBBLESTONE, 1));
			put(new ItemStack(Material.IRON_ORE, 1), new ItemStack(Material.IRON_INGOT, 1));
			put(new ItemStack(Material.REDSTONE_ORE, 1), new ItemStack(Material.REDSTONE, 4));
			put(new ItemStack(Material.DIAMOND_ORE, 1), new ItemStack(Material.EMERALD, 1));
			put(new ItemStack(Material.LAPIS_ORE, 1), new ItemStack(Material.INK_SACK, 4, 
					(short) DyeColor.BLUE.ordinal()));
		}
	
	};
	
	public static ItemStack getItemFromMaterial(ItemStack item) {
		for (ItemStack key : BLOCK_DROPS.keySet()) {
			if (key.isSimilar(item)) {
				return BLOCK_DROPS.get(key);
			}
		}
		return item;
	}

}
