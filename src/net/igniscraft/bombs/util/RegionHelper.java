package net.igniscraft.bombs.util;

import net.igniscraft.bombs.Core;

import org.bukkit.Location;

import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RegionHelper {
	
	private static final Core CORE = Core.getInstance();
	
	private RegionHelper() {}
	
	public static ProtectedRegion getRegionAtLocation(Location loc) {
		RegionManager manager = CORE.getWorldGuard().getRegionManager(loc.getWorld());
		for (ProtectedRegion region : manager.getApplicableRegions(loc)) {
			if (region.getId().toLowerCase().contains("mine")) {
				return region;
			}
		}
		return null;
	}
	
	public static PointLocator getBoundsOfRegion(Location loc) {
		ProtectedRegion region = getRegionAtLocation(loc);
		if (region == null) {
			return null;
		}
		return new PointLocator(region.getMinimumPoint(), region.getMaximumPoint());
	}

}
