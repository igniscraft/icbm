package net.igniscraft.bombs.util;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.igniscraft.bombs.bombs.*;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class ConfigHandler {
	
	private File fileConfig;
	private FileConfiguration config;
	
	public ConfigHandler(File fileConfig) {
		this.fileConfig = fileConfig;
		this.config = YamlConfiguration.loadConfiguration(fileConfig);
		init();
	}
	
	private void init() {
		for (String path : config.getConfigurationSection("bombs").getKeys(false)) {
			MineBombHandler.addMineBomb(getItemFromPath("bombs." + path), getMineBombFromPath("bombs." + path));
		}
	}
	
	public void reload() {
		config = YamlConfiguration.loadConfiguration(fileConfig);
		MineBombHandler.flush();
		init();
	}
	
	private ItemStack getItemFromPath(String path) {
		Material material = Material.AIR;
		short data = 0;
		try {
			String str = config.getString(path + ".materialType");
			if (str.contains(":")) {
				String[] strings = str.split("\\:");
				material = Material.valueOf(strings[0]);
				data = Short.valueOf(strings[1]);
			}
			
		} catch (NumberFormatException ex) {}
		return new ItemStack(material, 1, data);
	}
	
	private AbstractMineBomb getMineBombFromPath(String path) {
		switch (BombType.valueOf(config.getString(path + ".bombType"))) {
			case NORMAL:
				return new NormalMineBomb(ActivationType.valueOf(config.getString(path + ".activationType")), 
						(byte) config.getInt(path + ".radius"), config.getBoolean(path + ".willSmelt"), 
						config.getBoolean(path + ".addToInventory"), (byte) config.getInt(path + ".fortuneLevel"),
						getMaterialSet(config.getStringList(path + ".whitelist")), 
						getMaterialSet(config.getStringList(path + ".blacklist")));
			case LINEAR:
				return new LinearMineBomb(ActivationType.valueOf(config.getString(path + ".activationType")), 
						(byte) config.getInt(path + ".radius"), config.getBoolean(path + ".willSmelt"), 
						config.getBoolean(path + ".addToInventory"), (byte) config.getInt(path + ".fortuneLevel"),
						getMaterialSet(config.getStringList(path + ".whitelist")), 
						getMaterialSet(config.getStringList(path + ".blacklist")));
			case SQUARE:
				return new SquareMineBomb(ActivationType.valueOf(config.getString(path + ".activationType")), 
						(byte) config.getInt(path + ".radius"), config.getBoolean(path + ".willSmelt"), 
						config.getBoolean(path + ".addToInventory"), (byte) config.getInt(path + ".fortuneLevel"),
						getMaterialSet(config.getStringList(path + ".whitelist")), 
						getMaterialSet(config.getStringList(path + ".blacklist")));
			case GRID:
				return new GridMineBomb(ActivationType.valueOf(config.getString(path + ".activationType")), 
						(byte) config.getInt(path + ".radius"), config.getBoolean(path + ".willSmelt"), 
						config.getBoolean(path + ".addToInventory"), (byte) config.getInt(path + ".fortuneLevel"),
						getMaterialSet(config.getStringList(path + ".whitelist")), 
						getMaterialSet(config.getStringList(path + ".blacklist")));
			default:
				return null;
		}
	}
	
	private Set<Material> getMaterialSet(List<String> strs) {
		Set<Material> toReturn = new HashSet<>();
		for (String string : strs) {
			Material material = Material.valueOf(string);
			if (material != null) {
				toReturn.add(material);
			}
		}
		return toReturn;
	}

}
