package net.igniscraft.bombs.bombs;

public enum ActivationType {
	
	LEFT_CLICK,
	RIGHT_CLICK,
	DROP;

}
