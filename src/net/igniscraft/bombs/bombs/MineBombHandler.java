package net.igniscraft.bombs.bombs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

public class MineBombHandler {
	
	private static final Map<ItemStack, AbstractMineBomb> BOMBS = new HashMap<>();
	private static final Set<Item> PROTECTED_ITEMS = new HashSet<>();
	
	private MineBombHandler() {}
	
	public static void addMineBomb(ItemStack key, AbstractMineBomb value) {
		BOMBS.put(key, value);
	}
	
	public static void clearBombs() {
		BOMBS.clear();
	}
	
	public static AbstractMineBomb getBombFromItem(ItemStack item) {
		for (ItemStack itemStack : BOMBS.keySet()) {
			if (item.isSimilar(itemStack)) {
				return BOMBS.get(itemStack);
			}
		}
		return null;
	}
	
	public static void flush() {
		clearBombs();
		PROTECTED_ITEMS.clear();
	}
	
	public static void addProtectedItem(Item item) {
		PROTECTED_ITEMS.add(item);
	}
	
	public static void removeProtectedItem(Item item) {
		PROTECTED_ITEMS.remove(item);
	}
	
	public static boolean isProtectedItem(Item item) {
		return PROTECTED_ITEMS.contains(item);
	}

}
