package net.igniscraft.bombs.bombs;

import java.util.Set;

import net.igniscraft.bombs.util.PointLocator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class SquareMineBomb extends AbstractMineBomb {

	public SquareMineBomb(ActivationType activationType, byte radius, boolean willSmelt, boolean addToInventory, 
			byte fortuneLevel, Set<Material> whitelist, Set<Material> blacklist) {
		super(activationType, radius, willSmelt, addToInventory, fortuneLevel, whitelist, blacklist);
	}

	@Override
	public void detonate(Player player, Location loc) {
		Location clone = loc.clone();
		PointLocator locator = getBoundsOfRegion(clone);
		PointLocator locator2 = getBoundsOfSquare(locator, clone);
		World world = clone.getWorld();
		Location location;
		for (int y = -radius; y <= radius; y++) {
			for (int x = locator2.getMaxX(); x >= locator2.getMinX(); x--) {
				for (int z = locator2.getMaxZ(); z >= locator2.getMinZ(); z--) {
					location = new Location(world, x, clone.getBlockY() + y, z);
					if (!(locator.isWithinBounds(location))) continue;
					playSoundAndEffect(location);
					detonateBlock(player, location.getBlock());
				}
			}
		}
	}
	
	

}
