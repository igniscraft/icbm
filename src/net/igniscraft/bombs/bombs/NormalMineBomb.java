package net.igniscraft.bombs.bombs;

import java.util.Set;

import net.igniscraft.bombs.util.PointLocator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class NormalMineBomb extends AbstractMineBomb {

	public NormalMineBomb(ActivationType activationType, byte radius, boolean willSmelt, boolean addToInventory, 
			byte fortuneLevel, Set<Material> whitelist, Set<Material> blacklist) {
		super(activationType, radius, willSmelt, addToInventory, fortuneLevel, whitelist, blacklist);
	}

	@Override
	public void detonate(Player player, Location loc) {
		PointLocator locator = getBoundsOfRegion(loc);
		for (int tmp_radius = 0; tmp_radius <= radius; tmp_radius++) {
			for (int i = 0; i < 360; i++) {
				double radians = Math.toRadians(i);
				double u = tmp_radius * Math.cos(i);
				double x = Math.sqrt(tmp_radius * tmp_radius - u * u) * Math.cos(radians);
				double y = Math.sqrt(tmp_radius * tmp_radius - u * u) * Math.sin(radians);
				double z = u;
				Location location = new Location(loc.getWorld(), (int) (loc.getBlockX() + x + 1), 
						(int) (loc.getBlockY() + y + 1), (int) (loc.getBlockZ() + z + 1));
				if (!(locator.isWithinBounds(location))) continue;
				detonateBlock(player, location.getBlock());
			}
		}
	}

}
