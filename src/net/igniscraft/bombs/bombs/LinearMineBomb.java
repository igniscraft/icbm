package net.igniscraft.bombs.bombs;

import java.util.Set;

import net.igniscraft.bombs.util.PointLocator;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class LinearMineBomb extends AbstractMineBomb {
	
	public LinearMineBomb(ActivationType activationType, byte radius, boolean willSmelt, boolean addToInventory,
			byte fortuneLevel, Set<Material> whitelist, Set<Material> blacklist) {
		super(activationType, radius, willSmelt, addToInventory, fortuneLevel,
				whitelist, blacklist);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void detonate(Player player, Location loc) {
		Location clone = loc.clone();
		PointLocator locator = getBoundsOfRegion(clone);
		PointLocator locator2 = getBoundsOfSquare(locator, clone);
		World world = clone.getWorld();
		Location location;
		Location tmp;
		for (int y = locator.getMaxY(); y >= locator.getMinY(); y--) {
			tmp = new Location(world, loc.getBlockX(), y, loc.getBlockZ());
			world.playEffect(tmp, Effect.EXPLOSION_LARGE, null);
			world.playSound(tmp, Sound.EXPLODE, 14f, 0f);
			for (int x = locator2.getMaxX(); x >= locator2.getMinX(); x--) {
				for (int z = locator2.getMaxZ(); z >= locator2.getMinZ(); z--) {
					location = new Location(world, x, y, z);
					if (!(locator.isWithinBounds(location))) continue;
					detonateBlock(player, location.getBlock());
				}
			}
		}
	}

}
