package net.igniscraft.bombs.bombs;

import java.util.Set;

import net.igniscraft.bombs.util.PointLocator;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class GridMineBomb extends AbstractMineBomb {

	public GridMineBomb(ActivationType activationType, byte radius, boolean willSmelt, boolean addToInventory,
			byte fortuneLevel, Set<Material> whitelist, Set<Material> blacklist) {
		super(activationType, radius, willSmelt, addToInventory, fortuneLevel, whitelist, blacklist);
	}

	@Override
	public void detonate(Player player, Location loc) {
		Location clone = loc.clone();
		PointLocator locator = getBoundsOfRegion(clone);
		World world = clone.getWorld();
		for (int y = clone.getBlockY(); y >= clone.getBlockY() - radius; y--) {
			playSoundAndEffect(new Location(loc.getWorld(), loc.getBlockX(), y, loc.getBlockZ()));
			// If y range is out of bounds, break out of detonation
			if (!(locator.isWithinBounds(new Location(world, locator.getMinX(), y, locator.getMinZ())))) {
				break;
			}
			for (int x = locator.getMinX(); x <= locator.getMaxX(); x++) {
				for (int z = locator.getMinZ(); z <= locator.getMaxZ(); z++) {
					detonateBlock(player, world.getBlockAt(x, y, z));
				}
			}
		}
	}
	
	

}
