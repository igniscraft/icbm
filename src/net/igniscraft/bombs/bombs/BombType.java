package net.igniscraft.bombs.bombs;

public enum BombType {
	
	NORMAL,
	LINEAR,
	GRID,
	SQUARE;

}
