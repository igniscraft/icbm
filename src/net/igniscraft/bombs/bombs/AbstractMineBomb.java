package net.igniscraft.bombs.bombs;

import java.util.Random;
import java.util.Set;

import net.igniscraft.bombs.util.BlockDrops;
import net.igniscraft.bombs.util.MineBombRunnable;
import net.igniscraft.bombs.util.PointLocator;
import net.igniscraft.bombs.util.RegionHelper;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * The abstract implementation of a MineBomb, detonation to be implemented in subclasses
 */
public abstract class AbstractMineBomb {
	
	private static final Random RANDOM = new Random();

	private ActivationType activationType;
	protected byte radius;
	private boolean willSmelt;
	private boolean addToInventory;
	private boolean hasFortune;
	private byte fortuneLevel;
	private boolean hasWhitelist;
	private Set<Material> whitelist;
	private boolean hasBlacklist;
	private Set<Material> blacklist;

	/**
	 * Constructor
	 * @param activationType The way the MineBomb is activated
	 * @param radius The radius of the MineBomb
	 * @param willSmelt Whether or not blocks will be smelted
	 * @param addToInventory Whether or not drops will be added to inventory
	 * @param fortuneLevel Fortune level
	 * @param whitelist Blocks that are whitelisted
	 * @param blacklist Blocks that are blacklisted
	 */
	protected AbstractMineBomb(ActivationType activationType, byte radius, boolean willSmelt, boolean addToInventory,
			byte fortuneLevel, Set<Material> whitelist, Set<Material> blacklist) {
		this.activationType = activationType;
		this.radius = radius;
		this.willSmelt = willSmelt;
		this.addToInventory = addToInventory;
		this.fortuneLevel = fortuneLevel;
		this.hasFortune = (fortuneLevel != 0);
		this.whitelist = whitelist;
		this.hasWhitelist = !whitelist.isEmpty();
		this.blacklist = blacklist;
		this.hasBlacklist = !blacklist.isEmpty();
	}

	/**
	 * The method to handle the functionality of the explosion, implemented in subclasses
	 * @param player The player who detonated the bomb
	 * @param loc The location at which the bomb was activated
	 */
	public abstract void detonate(Player player, Location loc);
	
	/**
	 * Gets the ActivationType for this bomb
	 * @return The ActivationType
	 */
	public ActivationType getActivationType() {
		return this.activationType;
	}
	
	/**
	 * Forces the player to throw a bomb
	 * @param player The player to force to throw the bomb
	 */
	public void throwBomb(Player player) {
		ItemStack itemstack = player.getItemInHand();
		ItemStack clone = itemstack.clone();
		clone.setAmount(1);
		if (itemstack.getAmount() == 1) {
			player.getInventory().setItem(player.getInventory().first(itemstack), null);
		} else { 
			itemstack.setAmount(itemstack.getAmount() - 1);
		}
		Item item = player.getWorld().dropItemNaturally(player.getLocation().clone().add(0, 1.5, 0), clone);
		MineBombHandler.addProtectedItem(item);
		item.setVelocity(player.getEyeLocation().getDirection().multiply(0.8));
		new MineBombRunnable(player, item, this);
	}
	
	/**
	 * Applies fortune to the ItemStack using interesting random technique
	 * @param toDrop The ItemStack to be modified
	 */
	private void applyFortune(ItemStack toDrop) {
		int additional = RANDOM.nextInt(fortuneLevel - 1) + 1;
		toDrop.setAmount(toDrop.getAmount() + additional);
	}
	
	/**
	 * Detonates the block, and performs fortune and dropping of the corresponding block
	 * @param player The player to give the item to (if the option is set)
	 * @param block The block to detonate
	 */
	protected void detonateBlock(Player player, Block block) {
		ItemStack toDrop = destroyBlock(block);
		if (toDrop == null || toDrop.getType() == Material.AIR) return;
		if (hasFortune) applyFortune(toDrop);
		if (addToInventory) {
			player.getInventory().addItem(toDrop);
		} else {
			block.getWorld().dropItem(block.getLocation(), toDrop);
		}
	}
	
	/**
	 * Plays desired sound and effect at a location
	 * @param location The location to play the effect to
	 */
	protected void playSoundAndEffect(Location location) {
		location.getWorld().playEffect(location, Effect.EXPLOSION_HUGE, null);
		location.getWorld().playSound(location, Sound.EXPLODE, 14f, 0f);
	}
	
	/**
	 * Destroys a block, and returns it's ItemStack correspondent (with silk/smelt modifications)
	 * @param block The Block to destroy
	 * @return The ItemStack correspondent
	 */
	@SuppressWarnings("deprecation")
	private ItemStack destroyBlock(Block block) {
		if (hasWhitelist && !(whitelist.contains(block.getType()))) {
			return null;
		}
		if (hasBlacklist && blacklist.contains(block.getType())) {
			return null;
		}
		ItemStack base = null;
		if (willSmelt) {
			base = BlockDrops.getItemFromMaterial(new ItemStack(block.getType(), 1, block.getData()));
		} else {
			base = new ItemStack(block.getType(), 1, (short) block.getData());
		}
		block.setType(Material.AIR);
		return base;
	}
	
	/**
	 * A method to get a PointLocator representing the bounds of the region
	 * @param clone The Location to get the region from
	 * @return The PointLocator, which represents the bounds of the region
	 */
	protected PointLocator getBoundsOfRegion(Location clone) {
		PointLocator locator = RegionHelper.getBoundsOfRegion(clone);
		// If it's not, it's 1 y block above
		if (locator == null) {
			return RegionHelper.getBoundsOfRegion(clone.subtract(0, 1, 0));
		}
		return locator;
	}
	
	/**
	 * 
	 * @param locator The locator to get the Y values (of the region) from
	 * @param clone
	 * @return
	 */
	protected PointLocator getBoundsOfSquare(PointLocator locator, Location clone) {
		return new PointLocator(new Location(clone.getWorld(), clone.getBlockX() + radius, locator.getMinY(), 
				clone.getBlockZ() + radius), new Location(clone.getWorld(), clone.getBlockX() - radius, 
						locator.getMaxY(), clone.getBlockZ() - radius));
	}

}
