package net.igniscraft.bombs;

import net.igniscraft.bombs.bombs.AbstractMineBomb;
import net.igniscraft.bombs.bombs.MineBombHandler;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class BlockPlace implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent event) {
		ItemStack item = event.getItemInHand();
		Block block = event.getBlockPlaced();
		if (item == null || item.getType() == Material.AIR) {
			item = new ItemStack(block.getType(), 1, block.getData());
		}
		AbstractMineBomb mineBomb = MineBombHandler.getBombFromItem(item);
		if (mineBomb == null) {
			return;
		}
		event.setCancelled(true);
 	}

}
