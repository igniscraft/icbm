package net.igniscraft.bombs;

import net.igniscraft.bombs.bombs.AbstractMineBomb;
import net.igniscraft.bombs.bombs.ActivationType;
import net.igniscraft.bombs.bombs.MineBombHandler;
import net.igniscraft.bombs.util.MineBombRunnable;

import org.bukkit.ChatColor;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class DropItem implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		Item item = event.getItemDrop();
		ItemStack itemStack = item.getItemStack();
		AbstractMineBomb mineBomb = MineBombHandler.getBombFromItem(itemStack);
		if (mineBomb == null) {
			return;
		}
		Player player = event.getPlayer();
		if (mineBomb.getActivationType() != ActivationType.DROP) {
			player.sendMessage(ChatColor.RED + "This MineBomb can't be dropped!");
			event.setCancelled(true);
			return;
		}
		if (itemStack.getAmount() > 1) {
			player.sendMessage(ChatColor.RED + "You may only drop one Minebomb at a time!");
			event.setCancelled(true);
			return;
		}
		new MineBombRunnable(player, item, mineBomb);
	}

}
