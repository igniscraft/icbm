package net.igniscraft.bombs;

import java.io.File;

import net.igniscraft.bombs.util.ConfigHandler;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class Core extends JavaPlugin {
	
	private static Core instance;
	
	public static Core getInstance() {
		return instance;
	}
	
	private WorldGuardPlugin worldGuard;
	
	public WorldGuardPlugin getWorldGuard() {
		return worldGuard;
	}
	
	@Override
	public void onEnable() {
		instance = this;
		
		worldGuard = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
		
		new ConfigHandler(new File(getDataFolder(), "config.yml"));
		
		PluginManager pluginManager = Bukkit.getPluginManager();
		pluginManager.registerEvents(new PlayerInteract(), this);
		pluginManager.registerEvents(new DropItem(), this);
		pluginManager.registerEvents(new ItemProtector(), this);
		pluginManager.registerEvents(new BlockPlace(), this);
	}

}
