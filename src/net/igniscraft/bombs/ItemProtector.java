package net.igniscraft.bombs;

import net.igniscraft.bombs.bombs.MineBombHandler;

import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ItemProtector implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		Item item = event.getItem();
		if (MineBombHandler.isProtectedItem(item)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST) 
	public void onEntityDestroy(ItemDespawnEvent event) {
		Item item = event.getEntity();
		if (MineBombHandler.isProtectedItem(item)) {
			event.setCancelled(true);
		}
	}

}
